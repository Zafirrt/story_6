from django.urls import path, include
from .views import *

urlpatterns = [
	path('login-register/', login_register, name='login_register'),
	path('check/', check, name='check'),
	path('check2/', check2, name='check2'),
	path('hehe/', hehe, name='hehe'),
	path('auth/', include('social_django.urls', namespace='social')),
	path('logout/', logout, name='logout'),
]