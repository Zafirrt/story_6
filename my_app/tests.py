from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from .views import *
from .models import *

import re
import time

class TestFunctionalityStory8(LiveServerTestCase):

	def setUp(self):
		my_options = webdriver.ChromeOptions()
		my_options.add_argument('--headless')
		self.browser = webdriver.Chrome(chrome_options=my_options)

	def tearDown(self):
		self.browser.implicitly_wait(3)
		self.browser.quit()

	def test_if_color_is_same_throughout_pages(self):
		self.browser.get(self.live_server_url)
		time.sleep(5)
		body = self.browser.find_element_by_tag_name("body")
		prev_color = body.value_of_css_property("background-color")
		dark_theme = self.browser.find_element_by_id("theme-dark")
		dark_theme.click()
		time.sleep(1)
		body = self.browser.find_element_by_tag_name("body")
		new_color = body.value_of_css_property("background-color")
		self.assertNotEqual(prev_color, new_color)

	def test_does_local_storage_work(self):
		self.browser.get(self.live_server_url)
		time.sleep(5)
		dark_theme = self.browser.find_element_by_id("theme-dark")
		dark_theme.click()
		time.sleep(1)
		body = self.browser.find_element_by_tag_name("body")
		prev_color = body.value_of_css_property("background-color")
		self.browser.get("{}{}".format(self.live_server_url, '/profile/'))
		time.sleep(5)
		body = self.browser.find_element_by_tag_name("body")
		color = body.value_of_css_property("background-color")
		self.assertEqual(color, prev_color)

class TestFunctionalityStory7(LiveServerTestCase):

	def setUp(self):
		my_options = webdriver.ChromeOptions()
		my_options.add_argument('--headless')
		self.browser = webdriver.Chrome(chrome_options=my_options)

	def tearDown(self):
		self.browser.implicitly_wait(3)
		self.browser.quit()

	def test_status_message_can_work_and_show(self):
		self.browser.get(self.live_server_url)
		status_text_area = self.browser.find_element_by_id("id_status_message")
		status_text_area.send_keys("Mr Stark I dont feel so good :( (This is a Test)")	
		status_text_area.submit()
		time.sleep(3)
		self.assertIn("Mr Stark I dont feel so good :( (This is a Test)", self.browser.page_source)

	def test_form_size(self):
		self.browser.get(self.live_server_url)
		status_form = self.browser.find_element_by_id("id_status_message")
		status_form_height_value = status_form.value_of_css_property("height")
		status_form_width_value = status_form.value_of_css_property("width")
		self.assertIn('381px', status_form_width_value)
		self.assertIn('246px', status_form_height_value)

	def test_form_is_above_button(self):
		self.browser.get(self.live_server_url)
		form_is_above_regex = re.compile("<form method=\"post\">[\s\S]*<input id=\"sub_but\"")
		self.assertTrue(form_is_above_regex.search(self.browser.page_source))

	def test_new_status_is_above_form(self):
		self.browser.get(self.live_server_url)
		status_text_area = self.browser.find_element_by_id("id_status_message")
		status_text_area.send_keys("Bad Sad Mad Dad Cat")	
		status_text_area.submit()
		time.sleep(3)
		new_status_is_above_regex = re.compile("Bad Sad Mad Dad Cat[\s\S]*<form method=\"post\">")
		self.assertTrue(new_status_is_above_regex.search(self.browser.page_source))

class TestStory6(TestCase):

	def test_check_response(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_check_function_name(self):
		response = resolve('/')
		self.assertEqual(response.func, index)

	def test_check_url_name(self):
		response = resolve('/')
		self.assertEqual(response.url_name, 'index')

	def test_check_landing_page(self):
		response = Client().get('/')
		self.assertIn("<h3>Hello", response.content.decode('utf-8'))

	def test_check_templates_name(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_check_form_tag(self):
		response = Client().get('/')
		self.assertIn('</form>', response.content.decode('utf-8'))

	def test_check_post(self):
		response = Client().post('/', {"status_message": "test status"})
		self.assertIn('test status', response.content.decode('utf-8'))

	def test_check_database(self):
		before = Status.objects.all().count()
		Status.objects.create(
				status_message = "test status",
				time = datetime.datetime.now()
			)
		after  = Status.objects.all().count()
		self.assertEqual(before + 1, after)