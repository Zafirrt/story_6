from django import forms

class RegistrationForm(forms.Form):

	email		= forms.EmailField(min_length=3, label='Email')
	nama		= forms.CharField(label='Name')
	password	= forms.CharField(min_length=8, label='Password', widget=forms.PasswordInput())
	re_password	= forms.CharField(label='Repeat Password', widget=forms.PasswordInput())

class LoginForm(forms.Form):

	username = forms.CharField()
	password = forms.CharField(min_length=8, widget=forms.PasswordInput())