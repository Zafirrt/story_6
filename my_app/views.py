from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import *
from .models import *
import datetime

def index(request):
	if request.method == 'POST':
		status_form = StatusForm(request.POST)
		if status_form.is_valid():
			new_status = Status(
					status_message = status_form.cleaned_data['status_message'],
					time = datetime.datetime.now()
				)
			new_status.save()

	else:
		status_form = StatusForm()

	index_context = {'status_form': status_form, "all_status": Status.objects.all().values()[::-1]}
	return render(request, 'index.html', index_context)

def profile(request):
	profile_context = {}
	return render(request, 'profile.html', profile_context)	