from django.urls import path
from .views import *

urlpatterns = [
	path('', ajaxku, name='ajaxku'),
	path('json-data-1/', json_data_1, name='json-data-1'),
	path('update-session/', update_session, name='update-session'),
	path('get-session/', get_session, name='get-session'),
]
