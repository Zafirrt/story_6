from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from login_and_register.models import *
from login_and_register.forms import *
from django.contrib.auth.hashers import make_password
import json

my_regex = r".*"

def login_register(request):
	if request.method == 'POST':
		registerForm = RegistrationForm(request.POST)
		if(registerForm.is_valid()):
			subscriber = Subscriber(
					nama		= registerForm.cleaned_data['nama'],
					email		= registerForm.cleaned_data['email'],
					password	= make_password(registerForm.cleaned_data['password']),
				)
			subscriber.save()
			return HttpResponseRedirect('/')
	else:
		loginForm = LoginForm()
		registerForm = RegistrationForm()
	register_context = {'registerForm' : registerForm, 'loginForm': loginForm}
	return render(request, 'log_and_reg.html', register_context)

def check(request, **kwargs):
	email = request.POST.get('email')
	format = '@' in email
	if not(Subscriber.objects.filter(email=email).exists()) and format:
		return JsonResponse({'valid': 1})
	else:
		return JsonResponse({'valid': 0})

def check2(request, **kwargs):
	password = request.POST.get('password')
	re_password = request.POST.get('re_password')
	if password == re_password:
		return JsonResponse({'valid': 1})
	else:
		return JsonResponse({'valid': 0})

def logout(request):
	request.session.flush()
	return HttpResponseRedirect('/')

def hehe(request, **kwargs):
	if(request.method == 'POST'):
		account = request.POST.get('num')
		Subscriber.objects.filter(id=account).delete()
		return JsonResponse({})
	else:
		the_data = []
		all_data = Subscriber.objects.all()
		for i in all_data:
			temp = {}
			temp['id'] = i.id
			temp['email'] = i.email
			temp['nama'] = i.nama
			the_data.append(temp)
		return JsonResponse(the_data, safe=False)