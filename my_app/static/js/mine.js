jQuery(document).ready(function($) {
	
	var allPanels = $('.accordion > dd').hide();
	
	$('.accordion > dt > a').click(function() {
        allPanels.slideUp();
        if(!($(this).parent().next().is(':visible')))   {
        	$(this).parent().next().slideDown();
        }
    	return false;
	});
});


if(localStorage.getItem("theme") != null)	{
	document.getElementById("theme").href = localStorage.getItem("theme");
}

$('#theme-dark').click(function(){
	document.getElementById("theme").href = "/static/css/themes/dark.css";
	localStorage.setItem("theme", "/static/css/themes/dark.css");
});
$("#theme-dark").hover(
	function() {
		document.getElementById("theme").href = "/static/css/themes/dark.css";
	}, function() {
		document.getElementById("theme").href = localStorage.getItem("theme");
	}
);

$('#theme-light').click(function(){
	document.getElementById("theme").href = "/static/css/themes/light.css";
	localStorage.setItem("theme", "/static/css/themes/light.css");
});
$("#theme-light").hover(
	function() {
		document.getElementById("theme").href = "/static/css/themes/light.css";
	}, function() {
		document.getElementById("theme").href = localStorage.getItem("theme");
	}
);

setTimeout(function() {
	$("#loader").fadeOut();
	$("#transbox").fadeOut();
}, 3000);