from django import forms
from .models import *
import datetime

class StatusForm(forms.Form):
	attrs = {"class": "status-input"}
	status_message = forms.CharField(
		label='Status Today',
		required=True,
		widget=forms.Textarea(attrs=attrs),
	)
	class Meta:
		model = Status
		fields = ('status_message',)
		label = {
			"status_message" : "Status Today"
		}

