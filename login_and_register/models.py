from django.db import models
import django.db.models.options as options

options.DEFAULT_NAMES = options.DEFAULT_NAMES + ('time_created',)

import datetime
import pytz

timezone = pytz.timezone("Asia/Jakarta")

class Subscriber(models.Model):

	email 		= models.EmailField()
	nama  		= models.TextField()
	password	= models.TextField()

	class Meta:
		verbose_name = "Subscriber"
		verbose_name_plural = "Subscribers"