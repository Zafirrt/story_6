from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import re
import time

class FunctionalTest(LiveServerTestCase):

	def setUp(self):
		my_options = webdriver.ChromeOptions()
		my_options.add_argument('--headless')
		self.browser = webdriver.Chrome(chrome_options=my_options)

	def tearDown(self):
		self.browser.implicitly_wait(3)
		self.browser.quit()

	def test_add_to_wishlist_works(self):
		self.browser.get("{}{}".format(self.live_server_url, '/ajaxku/'))
		time.sleep(5)
		wishlist_button = self.browser.find_element_by_id("item0")
		wishlist_button.click()
		time.sleep(3)
		cost = self.browser.find_element_by_id("cost").text
		self.assertIn("Remove from wishlist!", self.browser.page_source)
		self.assertTrue(int(cost[3:]) > 0)