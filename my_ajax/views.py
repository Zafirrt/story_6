from django.shortcuts import render
from django.http import JsonResponse, HttpResponseNotAllowed, HttpResponse
import urllib, json

# Create your views here.

def json_data_1(request):
	data = json.loads(urllib.request.urlopen('https://enterkomputer.com/api/product/notebook.json').read())
	return JsonResponse(data, safe=False)

def update_session(request, **kwargs):
	if not request.is_ajax() or not request.method=='POST':
		return HttpResponseNotAllowed(['POST'])

	request.session['harga'] = request.POST.get('global_cost')
	return HttpResponse('ok')

def get_session(request):
	if request.session.has_key('harga'):
		return HttpResponse(request.session['harga'])
	else:
		return HttpResponse("0")

def ajaxku(request):
	return render(request, 'ajaxku.html')