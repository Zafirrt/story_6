
var global_cost = 0;
var global_data = [];

$(document).ready(function () {
	$.ajax({
		url: '/ajaxku/json-data-1',
		type: 'GET',
		data: {},
		dataType: "json",
		success: function (data) {
			global_data = Array.from(data);		
			$('#data_place').html("<table>");
			for(var i = 0; i < 100; ++i)	{

				$('#data_place').append('<tr>');
				val  = "<td>";
				val += '<div>Name: ' + global_data[i].name + '</div>';
				val += '<div>Brand: ' + global_data[i].brand_description + '</div>';
				val += '<div>Price: ' + global_data[i].price + '</div>';
				val += '<div>Weight: ' + global_data[i].weight + '</div>';
				val += "</td>";
				$('#data_place').append(val);

				val  = "<td>";
				val += '<div>Quantity: ' + global_data[i].quantity + '</div>';
				val += '<div>Details: ' + global_data[i].details + '</div>';
				val += '<div>Tokopedia: ' + global_data[i].link_toped + '</div>';
				val += '<div>Shopee: ' + global_data[i].link_shopee + '</div>';
				val += "</td>";
				$('#data_place').append(val);

				$('#data_place').append('</tr>');
				val = '<tr><td><input type=\"button\" id=\"item' + i + '\" value=\"Add to wishlist!\" onclick=\"buy(this.id)\"></tr></td>'
				$('#data_place').append(val);
			}
			$('#data_place').append("</table>");
		},
	});
});

function buy(data) {
	var num = parseInt(data.slice(4),10);
	global_cost += parseInt(global_data[num].price, 10);
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = "harga=" + global_cost + "expires=" + expires;
	$("#cost").html("Rp " + global_cost.toString(10));

	global_data[num].price *= -1;
	if(global_data[num].price < 0)
		$("#" + data).val("Remove from wishlist!");
	else
		$("#" + data).val("Add to wishlist!");

}