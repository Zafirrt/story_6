from django.db import models

# Create your models here.
class Status(models.Model):
	status_message = models.TextField(max_length=300)
	time = models.DateTimeField(auto_now_add=True, blank=True)