from django.apps import AppConfig


class MyAjaxConfig(AppConfig):
    name = 'my_ajax'
