from django.test import TestCase, Client
from login_and_register.models import *

class RegisterTest(TestCase):

	def test_if_works(self):
		before = Subscriber.objects.all().count()
		response = Client().post('/login-register/', {"email": "ya@ya.com", "nama": "YA", "password": "hehehehe", "re_password": "hehehehe"})
		after = Subscriber.objects.all().count()
		self.assertEqual(before + 1, after)

